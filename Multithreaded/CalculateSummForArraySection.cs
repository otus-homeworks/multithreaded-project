﻿namespace OtusTeaching.Multithreaded
{
    static class CalculateSummForArraySection
    {
        static public int Calculate(int[] array, int frInd, int toInd)
        {
            int summ = 0;
            for (int i = frInd; i < toInd; i++)
                // Типа с какой-то обработкой.
                summ += 2 * array[i] + 10;

            return summ;
        }
    }
}
