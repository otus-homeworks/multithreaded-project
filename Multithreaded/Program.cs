﻿using System;
using System.Diagnostics;

namespace OtusTeaching.Multithreaded
{

    class Program
    {
        static void Main(string[] args)
        {
            const int N = 100000; // Array size.
            
            Stopwatch stopWatch = new();
            GenerateArray gArray = new(N);
            Console.WriteLine($"Массив из {N} элементов сгенерирован.\n");

            stopWatch.Start();
            int summForUsual = UsualCalculateSumm.GetSumm(gArray.DataArray, N);
            stopWatch.Stop();
            Console.WriteLine($"Для обычного вычисления: сумма = {summForUsual}, время вычисления {stopWatch.Elapsed.TotalMilliseconds} \n");

            stopWatch.Reset();
            stopWatch.Start();
            int summForThreads = CalculateSummByThreads.GetSumm(gArray.DataArray, N, 5);
            stopWatch.Stop();
            Console.WriteLine($"Для вычисления c List<Thread>: сумма = {summForThreads}, время вычисления {stopWatch.Elapsed.TotalMilliseconds} \n");

            stopWatch.Reset();
            stopWatch.Start();
            int summForPLINQ = CalculateSummByPLINQ.GetSumm(gArray.DataArray);
            stopWatch.Stop();
            Console.WriteLine($"Для вычисления c PLINQ: сумма = {summForPLINQ}, время вычисления {stopWatch.Elapsed.TotalMilliseconds} \n");

            if (!CheckSumms(summForUsual, summForThreads, summForPLINQ))
                throw new ApplicationException("Суммы, вычисленные разными методами несовпадают !!!");

            Console.WriteLine("Нажмиту Enter для завершения");
            Console.ReadLine();
        }

        private static bool CheckSumms(int summForUsual, int summForThreads, int summForPLINQ)
        {
            return (summForUsual == summForThreads && summForUsual == summForPLINQ && summForThreads == summForPLINQ);
        }        
    }
}
