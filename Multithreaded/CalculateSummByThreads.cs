﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace OtusTeaching.Multithreaded
{
    class CalculateSummByThreads
    {
        private static int[] _array;
        private static int _N;
        private static int _threadsNum;
        private static List<int> _SummsCalculatedByThreads;
        private static int _WorkingThreadsNum;

        public static int GetSumm(int[] array, int N, ushort threadsNum)
        {
            if (N % threadsNum != 0)
                throw new ArgumentException("Количество потоков в CalculateSummByThreads.GetSumm следует задавать таким, чтобы количество элементов в обрабатываемом массиве нацело делилось на него.");

            _array = array;
            _N = N;
            _threadsNum = threadsNum;
            List<Thread> Threads = new(N);
            _SummsCalculatedByThreads = new();

            for (ushort i = 0; i < threadsNum; i++)
            {
                _SummsCalculatedByThreads.Add(0);
                Threads.Add(new Thread(CalcSummByThread));
            }

            _WorkingThreadsNum = threadsNum;
            for (ushort i = 0; i < threadsNum; i++)
                Threads[i].Start(i);

            // Ждём завершения всех потоков. После тормозов с Barrier на лекции? сделал своё.            

            Stopwatch stopWatch = new();
            stopWatch.Start();
            while (_WorkingThreadsNum > 0) ;

            stopWatch.Stop();
            Console.WriteLine($"время ожидания завершения всех потоков в List<Thread> {stopWatch.Elapsed.TotalMilliseconds} ");

            int totalSumm = 0;
            for (ushort i = 0; i < threadsNum; i++)
                totalSumm += _SummsCalculatedByThreads[i];

            return totalSumm;
        }

        public static void CalcSummByThread(object threadN)
        {
            ushort nThread = (ushort)threadN;

            // Т.к. из _array только считываю - никакие блокировки и синхронизаторы не использую.
            int lengthOfArraySectionToBeProcessed = _N / _threadsNum;

            int indForBeginProcessed = nThread * lengthOfArraySectionToBeProcessed; // Включительно.
            int indForEndProcessed = (nThread + 1) * lengthOfArraySectionToBeProcessed;   // Не включая.

            _SummsCalculatedByThreads[nThread] = CalculateSummForArraySection.Calculate(_array, indForBeginProcessed, indForEndProcessed);

            Interlocked.Decrement(ref _WorkingThreadsNum);
        }
    }
}
