﻿namespace OtusTeaching.Multithreaded
{
    class UsualCalculateSumm
    {
        public static int GetSumm(int[] array, int N)
        {
            return CalculateSummForArraySection.Calculate(array, 0, N);
        }
    }
}
