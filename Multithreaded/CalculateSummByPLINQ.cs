﻿using System.Linq;

namespace OtusTeaching.Multithreaded
{
    class CalculateSummByPLINQ
    {
        public static int GetSumm(int[] array)
        {
            return array.AsParallel().Sum(e => 2*e + 10);            
        }
    }
}
