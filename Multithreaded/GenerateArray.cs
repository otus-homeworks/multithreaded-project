﻿using System;

namespace OtusTeaching.Multithreaded
{
    class GenerateArray
    {        
        public int[] DataArray;
        private readonly Random rnd = new();

        public GenerateArray(int N)
        {            
            DataArray = new int[N];
            for (int i = 0; i < N; i++)
                // Диапазон задания случайных значений небольшой чтобы не думать о переполнении при вычислении суммы
                DataArray[i] = rnd.Next(-20000, 20000);
        }
    }
}
